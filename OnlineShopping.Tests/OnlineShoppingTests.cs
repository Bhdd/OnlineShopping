﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace OnlineShopping.Tests
{
    [TestClass]
    public class OnlineShoppingTests
    {
        [TestMethod]
        public void CheckCategories()
        {
            OnlineShopping.Repository.GenericUnitOfWork unitOfWork = new Repository.GenericUnitOfWork();
            var list = unitOfWork.GetRepositoryInstance<DAL.Tbl_Product>().GetAllRecords();

            foreach (DAL.Tbl_Product c in list)
            {
                Assert.AreNotEqual(null, c.CategoryId);
            }

        }
    }
}
